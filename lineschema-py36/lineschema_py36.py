import os

# txtファイル指定
dir = "../talk.txt"

# エラー処理
if not os.path.isfile(dir):
    print("[ERROR] NO FILE")

# 読み取りモード
file = open(dir, "r")

# 1行ずつ配列に格納
lines = file.readlines()


class Message:
    """トーク内の各メッセージ用クラス"""
    def __init__(self, *args, **kwargs):
        # instance values
        # 日付
        self.date = ""
        # 時刻
        self.time = ""
        # 送信者
        self.name = ""
        # メッセージ文字列
        self.str = ""

    def clear(self):
        self.name = ""
        self.str == ""


msg = Message()
for line in lines:

    if line == "\n":
        break

    if line[4] == '/':
        msg.date = line[0:10]
        continue

    if line[2] == ':':
        msg.time = line[0:5]

        msg.clear()

    else:
        msg.str += line

    print(msg.date, msg.time, msg.name, msg.str)


file.close()